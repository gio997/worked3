package com.example.myapplication003

import android.app.Person
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView




class RecyclerViewPersonAdapter(private  val personList: List<Person>) : RecyclerView.Adapter<RecyclerViewPersonAdapter.personViewHolder>() {

    class personViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private var imageView: ImageView = itemView.findViewById(R.id.imageView)
        private lateinit var textView1: TextView
        private lateinit var textView2: TextView

      init {
          textView1 = itemView.findViewById(R.id.textView)
          textView1 = itemView.findViewById(R.id.textView2)
      }
        fun setData(person: Person) {

            textView1.text = person.title

            textView1.text = person.description

            Glide.with(itemView)
                .lod(person.imageUrl)
                .into(imageView)

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): personViewHolder {
       val itemview =
           LayoutInflater.from(parent.context).inflate(R.layout.person_item, parent, false)
        return personViewHolder(itemview)
    }

    override fun onBindViewHolder(holder: personViewHolder, position: Int) {
        holder.setData(personList[position])


    }

    override fun getItemCount(): Int {
        return personList.size
    }
}
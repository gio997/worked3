package com.example.myapplication003

import android.app.Person
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity() : AppCompatActivity() {
    private lateinit var recyclerView: RecyclerView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        recyclerView = findViewById(R.id.recyclerView)
        recyclerView.layoutManager =  LinearLayoutManager(this)
    }

    private fun getPersonData() : List<Person> {

        val personList = ArrayList<Person>()

        personList.add(

            person(
                1,
                "https://www.auto-data.net/images/f108/Mercedes-Benz-190-W201.jpg",
                "Mercedes e190",
                "merccedes benz"

            )
        )
        personList.add(

            person(
                1,
                "https://www.auto-data.net/images/f108/Mercedes-Benz-190-W201.jpg",
                "Mercedes e190",
                "merccedes benz"

            )
        )
        personList.add(

            person(
                1,
                "https://www.auto-data.net/images/f108/Mercedes-Benz-190-W201.jpg",
                "Mercedes e190",
                "merccedes benz"

            )
        )
        personList.add(

            person(
                1,
                "https://www.auto-data.net/images/f108/Mercedes-Benz-190-W201.jpg",
                "Mercedes e190",
                "merccedes benz"

            )
        )
        personList.add(

            person(
                1,
                "https://www.auto-data.net/images/f108/Mercedes-Benz-190-W201.jpg",
                "Mercedes e190",
                "merccedes benz"

            )
        )
        personList.add(

            person(
                1,
                "https://www.auto-data.net/images/f108/Mercedes-Benz-190-W201.jpg",
                "Mercedes e190",
                "merccedes benz"

            )
        )
        personList.add(

            person(
                1,
                "https://www.auto-data.net/images/f108/Mercedes-Benz-190-W201.jpg",
                "Mercedes e190",
                "merccedes benz"

            )
        )
        personList.add(

            person(
                1,
                "https://www.auto-data.net/images/f108/Mercedes-Benz-190-W201.jpg",
                "Mercedes e190",
                "merccedes benz"

            )
        )

        return  personList


    }

}


}
